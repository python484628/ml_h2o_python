



# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# December 2021




# Install h20 if it's necessary
!pip install h2o


# See modules on computer
pip freeze


# Installing H20-3
# run the following commands in a terminal window to install H20 for python
# Install dependencies if needed : pip install requests, pip install tabulate, pip install "colorama>=0.3.8", pip install future

# run the following command to remove any existing H20 module for python (append with --user if needed): pip uninstall H20
# use pip install -f http://h2o-release.s3.amazonaws.com/h2O/latest_stable_Py.html h2o



# Load library
import h2o

# Start cluster
h2o.init()
# If it finds a cluster it will connect to it
# Then it gives information about date, version, number of nodes, ...
# You can check out the server UI at http://127.0.0.1:54321


# Check status of our cluster (how many resources, is it working, ...)
h2o.cluster().show_status()



# List of objects that we have in our server
h2o.ls()


# Load datasets from sklearn
from sklearn import datasets

# Load breast_cancer() data
data = datasets.load_breast_cancer()

# Define X and y variables
X = data.data
y = data.target


# Load pandas to manipulate dataframe
import pandas as pd


# Create dataframe
dataf = pd.DataFrame(X, columns=[data.feature_names])
# Create Target columns
dataf['Target'] = pd.Series(data=y, index=dataf.index)

# Print df
dataf



# Create dataframe with H2O
df = h2o.H2OFrame(dataf, header=1)


# Check number of rows
df.nrows # 569

# Check first rows of df
df.head()


# Check last rows of df
df.tail()

# Print summary of df
df.show_summary()

# Get to know the number of instances for the two classes that we have in Target column
# For class 0 (no cancer) we have 212 people
# For class 1 (cancer) we have 357 people
df[-1].table()



# If you want to open a file from a website you can use the following command with import_file
#df2 = h2o.import_file("http://s3.amazonaws.com/h2o-public-test-data/smalldata/prostate/prostate.csv.zip")




# Calculate mean for each column
colmeans = df.mean()
colmeans



# Load H2OGradientBoostingEstimator
from h2o.estimators.gbm import H2OGradientBoostingEstimator


# Start GBM
m = H2OGradientBoostingEstimator(ntrees=10, max_depth=5)

# On regarde si la colonne Target est en facteur ou pas

# Let's have a look at the Target column to get to know if it's a factor or not
df[-1].isfactor()
# False
# Therefore, we'll do a regression and not a classification because Target is an integer and not a factor


# Train model 
m.train(x=df.names[0:-1], y=df.names[-1], training_frame=df)

# Print model type 
print(m.type) 
# regression




# Now we can convert Target column into factor to realize a classification
df[-1] = df[-1].asfactor()

# Check if it's converted
df[-1].isfactor()
# True


# Train classification model
m.train(x=df.names[0:-1], y=df.names[-1], training_frame=df)

# Print model type
print(m.type) 
# classifier




# Show information about the training model
m.show()
# We have the confusion matrix, error by class, ...




# Show performance on training data
m.model_performance() 



# If we had a dataset for testing we could do the following command : 
m.model_performance(test_data=df)
# Here we use the same dataset for training and testing, but of course the idea is to use another dataset for the test


# We can do prediction with the following command : 
p = m.predict(df[:-1])
# Here, we use the same dataset again, so the idea is to use another dataset for prediction


# We can print the predictions
p['predict'].table()
# So it predicted 205 times the class 0 (no cancer) and 364 times the class 1 (cancer)


# We can print the model accuracy with the following :
m.accuracy()



# We can also use scikit-learn to measure the model's performance
from sklearn import metrics
print(metrics.accuracy_score(y, p.as_data_frame()['predict']))
# p is an h2o dataframe, that's why we use as_data_frame to have a pandas dataframe


# Stop cluster
h2o.cluster().shutdown()



